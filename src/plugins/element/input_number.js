import { ElInputNumber } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElInputNumber = ElInputNumber;
}
