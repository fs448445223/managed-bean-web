import { ElSelect, ElOption } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElSelect = ElSelect;
  vueComponent.components.ElOption = ElOption;
}
