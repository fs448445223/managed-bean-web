import { ElButton } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElButton = ElButton;
}
