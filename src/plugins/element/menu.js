import { ElMenu, ElMenuItem } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElMenu = ElMenu;
  vueComponent.components.ElMenuItem = ElMenuItem;
}
