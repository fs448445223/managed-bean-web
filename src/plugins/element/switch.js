import { ElSwitch } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElSwitch = ElSwitch;
}
