import { ElForm, ElFormItem } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElForm = ElForm;
  vueComponent.components.ElFormItem = ElFormItem;
}
