import { ElContainer, ElAside, ElHeader, ElMain, ElFooter } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElContainer = ElContainer;
  vueComponent.components.ElAside = ElAside;
  vueComponent.components.ElHeader = ElHeader;
  vueComponent.components.ElMain = ElMain;
  vueComponent.components.ElFooter = ElFooter;
}
