import { ElInput } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElInput = ElInput;
}
