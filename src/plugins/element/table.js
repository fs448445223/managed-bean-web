import { ElTable, ElTableColumn } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElTable = ElTable;
  vueComponent.components.ElTableColumn = ElTableColumn;
}
