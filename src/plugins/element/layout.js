import { ElRow, ElCol } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElRow = ElRow;
  vueComponent.components.ElCol = ElCol;
}
