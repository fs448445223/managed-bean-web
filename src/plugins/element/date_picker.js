import { ElDatePicker, ElTimeSelect } from 'element-plus';

export default function (vueComponent) {
  vueComponent.components = vueComponent.components || {};
  vueComponent.components.ElDatePicker = ElDatePicker;
  vueComponent.components.ElTimeSelect = ElTimeSelect;
}
