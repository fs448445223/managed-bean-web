import { createApp } from 'vue';
import { ElDialog, ElLoading, ElTooltip } from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';

import App from './App.vue';

import router from './router';
import store from './store';

const app = createApp(App);
app.use(router);
app.use(store);

app.use(ElDialog);
app.use(ElLoading);
app.use(ElTooltip);

app.mount('#app');

export default app;
