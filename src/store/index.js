import { createStore } from 'vuex';

export default createStore({
  state () {
    return {
      loading: false
    };
  },
  mutations: {
    setLoading(state) {
      state.loading = true;
    },
    unsetLoading(state) {
      state.loading = false;
    }
  }
});
