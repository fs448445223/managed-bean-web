import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router';

import Login from '../views/Login.vue';
import Console from '../views/Console.vue';

const routes = [
  {
    path: '/',
    name: 'login',
    component: Login
  },
  {
    path: '/model',
    name: 'console',
    component: Console,
    children: [
      {
        path: 'list',
        name: 'model_list',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "model_list" */ '../views/ModelList.vue')
      },
      {
        path: 'edit',
        name: 'model_edit',
        component: () => import(/* webpackChunkName: "model_edit" */ '../views/ModelEdit.vue')
      },
      {
        path: 'view',
        name: 'bean_list',
        props: true,
        component: () => import(/* webpackChunkName: "bean_list" */ '../views/BeanList.vue')
      },
      {
        path: 'bean-edit',
        name: 'bean_edit',
        props: true,
        component: () => import(/* webpackChunkName: "bean_edit" */ '../views/BeanEdit.vue')
      }
    ]
  }
];

const base = import.meta.env.BASE_URL;
const router = createRouter({
  history: ((history && history.back) instanceof Function) ? createWebHistory(base) : createWebHashHistory(base),
  routes
});

export default router;
