import vue from '@vitejs/plugin-vue';

/**
 * @type {import('vite').UserConfig}
 */
export default {
  server: {
    hmr: {
      port: 443,
      path: "/__hmr"
    }
  },
  plugins: [vue()]
};
